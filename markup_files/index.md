![Bild 1](media/1.jpeg)

**Ett totalt förändrat Slussen beräknas vara klart 2020.**

# Virtuellt byggande ledstjärna när nya Slussen växer fram

> ##### Slussen-projektet är ett gigantiskt projekt i alla avseenden. Från början har det varit klart att Informationsmodeller ska användas genom hela processen. Att utse en AIM/BIM Ansvarig hos varje konsult har visat sig vara ett lyckokast.

PROJEKTET STYRS AV STOCKHOLMS STAD och Exploateringskontoret och de som arbetar i beställarorganisationen är konsulter från många olika konsultföretag. Anläggningen beräknas vara klar 2020 och redan nu omfattar projektkontoret ett 50-tal personer. En av dem är Johanna Bodsten som arbetat i projektet under två år, de senaste ett och halvt åren som VDC Manager. Som sådan konkretiserar hon arbetet med strategiska frågor kring Virtuellt byggande.
​	Johanna Bodsten ingår i en grupp om fem personer som tillhör projektstabens IT-avdelning och som arbetar med hela projektet. En av de fem är ansvarig för hela gruppen, en har huvudansvar för projektportal och dokumenthantering, en
arbetar med fokus på CAD/ritningar och en är VDC Samordnare som har hand om modellsamordning i projekteringen. VDC Samordnaren hjälper projekteringsledarna att se till att det projekterade materialet fungerar bra tillsammans och
uppfyller de fastställda kraven. 
​	Våren 2011 bildade Johanna Bodsten och några andra personer med goda kunskaper i det som fysiskt händer inom planering och produktion, en arbetsgrupp i projektledningen.
​	– Alla i projektet har insett att 3D är bra och det vore dumt att bara använda modellerna i projekteringen. Vid ett sådant här stort multidisciplinärt projekt kan man spara kostnader i senare skeden om man använder modellerna genom hela
processen och inte tar fram information gång på gång. Gruppen ansåg det som möjligt att samordna och göra automatiska kollisionskontroller i projekteringen, att granska
systemhandlingen så digitalt som möjligt och att producera ritningar så långt som möjligt ur modellerna.
​	Arbetsgruppen diskuterade även att göra skedesplanering och 4D simuleringar utifrån modellen liksom att utnyttja modellerna till kalkylering, så kallad 5D kalkyl, både under projekteringens gång, i upphandlingsskedet och i produktionen.
Gruppen kom även fram till att det vore bra att ha armering i 3D med stöd av time-location; alltså att den kommer färdigbockad och kodad till byggarbetsplatsen, klar för montering.
​	– Under processens gång utnyttjade vi våra kontakter och träffade olika personer från branschen med erfarenhet av dessa processer. Vi tog även till vara tidigare erfarenheter av Virtuellt byggande i andra projekt, inte bara vad som gjorts utan snarare det man tror är möjligt att göra. Det mest spännande med mitt jobb är att jobba i en framtid vi ännu inte vet något om, säger Johanna Bodsten.
​	Ledningsgruppen sa ok till beslutsunderlaget och just nu projekteras en Systemhandling som ska granskas digitalt. Utifrån de definierade processerna ställdes krav på konsulterna hur modellerna ska se ut. För att kvalitetssäkra arbetet fick alla företag som arbetar med projekteringen utse en AIM/BIM Ansvarig, vars uppgift är att ansvara för de modeller som produceras från respektive teknikområde, se till att de uppfyller kraven, informera sina kollegor, ha kontakt med ledningen och träffa de andra ansvariga.
​	– Dessa personer, i dagsläget ett tiotal, har fått en viktig roll och är högt respekterade. Konstruktörer, arkitekter, landskapsarkitekter och installatörer hjälper varandra för att hela tiden bli bättre och det samarbetet ger ett brett perspektiv.
​	Den AIM/BIM Ansvarige kompletterar den traditionella Datasamordnaren. En VDC Samordnare deltar på projekteringsstyrningsmöten och granskar att det som de AIM/BIM Ansvariga lämnar in håller måttet. Ett av syftena med VDC Samordnare på beställarsidan är att ha direktkontakt med dem som projekterar.
​	– Detta är ett bra upplägg och en av de bästa idéer som vi genomfört, säger Johanna Bodsten. Det har gett otroligt positiv spin off.

NÄR INFORMATIONSMODELLERNA BÖRJADE PROJEKTERAS för cirka ett år sedan var många helt gröna när det gäller att skapa så här avancerade modeller, men tillsammans har de kunnat hjälpa varandra och projektledningen framåt. Även beställaren har haft stor nytta av att träffa dem som är bäst hos konsulterna samtidigt som de AIM/BIM Ansvariga har fått lära sig nya saker och bli ännu duktigare.
​	– Det är ett jätteroligt gäng att jobba med, vi får direktkontakt in till kärnan i varje konsultföretag. Våra projekteringsledare behöver inte ha kontakt med deras uppdragsledare för att lösa svåra datatekniska frågor som de varken kan eller ska förstå sig på. Detta är vårt jobb och det som vi är duktiga på. 
​	En av Johanna Bodstens arbetsuppgifter är att kontinuerligt kompetensutveckla projektledningen. Av de femtio personerna hade en del inte ens kommit i kontakt med 3D tidigare. Våren 2012 arrangerades en heldag för projektkontoret då bland annat begreppet Virtuellt byggande med dess delar Process, Organisation och Produkt gicks igenom, liksom strategi för VDC framöver och hur man inom VDC definierar olika
nivåer på Informationsmodeller.
​	– Det är organisationen och människorna i den som gör att man kan lyckas. Därför är det viktigt att alla förstår vad det hela handlar om. Målet är att organisationen ska bli självgående. 
​	Det var en uttalad strategi från början att tidigt börja med Samordningsmodeller för att säkerställa att när läget är skarpt, så ska alla konsulter ha rätt kunskapsnivå.
​	– Vi vet att programmen pratar med varandra, vi har löst de datatekniska problemen och är nu, efter ett år av förberedelser, i skarpt läge. Projekteringen fortsätter och vi börjar närma oss slutet av Systemhandlingen.

INFORMATIONSMODELLERNA SKA KUNNA IMPORTERAS direkt in i kalkyleringsprogram. Kostnad länkas alltså till modellerna, vilket här kallas 5D kalkylering. Informationsmodellerna ska även användas för att visa hur anläggningen ska byggas och i vilken tidsordning. 4D Simuleringen kan användas internt i
projektet och för att tydligt visa allmänheten hur anläggningen kommer att se ut.
​	En av de stora frågorna som ska lösas inom den närmaste tiden är hur granskningen av Systemhandlingen ska göras.
​	– Vi har beslutat att all granskning av Systemhandlingen ska ske utifrån en Granskningsmodell med tillhörande detaljritningar. En Granskningsmodell kan jämföras med en Samordningsmodell, fast istället för att vara tänkt till kollisionskontrollen är den anpassad utifrån vad som är av intresse för varje
granskande part. Det stora arbetet som kvarstår är att definiera processen för arbetet och hur Granskningsmodellen ska se ut.
​	En annan fråga som ska lösas under hösten 2012 gäller upphandlingen. Det är ett avancerat projekt att upphandla och det kan göras på olika sätt. Ytterligare en fråga som ska jobbas vidare med är kalkylering och även där är tanken att Informationsmodellerna ska utnyttjas ordentligt.

UNDER DET NÄRMASTE HALVÅRET ska Johanna Bodsten och hennes kollegor ta sig an två viktiga frågor – hur ska modellerna se ut för att fungera väl för entreprenörerna och för att fungera väl i förvaltningen.
​	– Det är jätteviktiga frågor som vi måste ta itu med direkt. Anläggningen ska vara klar 2020 men ingen vet hur förvaltningssystemen ser ut då. Det är en utmaning att titta in i framtiden och fastslå strategier så tidigt som möjligt. Framöver fokuserar jag alltså mer och mer på strategier. Men allt eftersom strategierna blir fastlagda blir organisationen mer och mer självgående. Arbetet är jättespännande och det är ett roligt projekt att delta i. 